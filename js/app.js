$(document).ready(function () {

  $('#search').click(function () {
    makeSearch();
  });

  $('#searchInput').keypress(function (e) {
    if (e.which == 13) {
      makeSearch();
      return false;
    }
  });

  $(document).on("click", ".addBtn", function (e) {

    var id = $(this).parent().attr('id');

    fetchData(id);
  });

  $(document).on("click", ".more-info", function (e) {

    var tooltip = $(e.target).children();

    $.each($('.more-info-tooltip'), function () {

      if (!$('.more-info-tooltip').hasClass('hidden') && !tooltip) {
          $('.more-info-tooltip').addClass('hidden');
      }

    });
    
    $.each(tooltip, function () {
      if (tooltip.hasClass('more-info-tooltip')) {
        if (tooltip.hasClass('hidden')) {
          tooltip.removeClass('hidden');
        } else {
          tooltip.addClass('hidden');
        }
      } else {
        return false;
      }
    });
  });

  function makeSearch () {
    $('#result-list').empty();
    $('#result').addClass('hidden');
    var searchInput = $('#searchInput').val();

    var query = { 
      q: searchInput
    }

    if (searchInput) {
      $.getJSON("http://www.omdbapi.com/?s=" + query.q)
      .done(function(data) {

        showList(data);

      });
    };
  }

  function fetchData (id) {
    var searchInput = id;

    var query = { 
      q: searchInput
    }

    if (searchInput) {
      $.getJSON("http://www.omdbapi.com/?i=" + query.q)
      .done(function(data) {

        $('<li/>', {
          'class': 'result-item',
          'id': 'list-item-' + data.imdbID,
          'data-imdb-id': data.imdbID,
          'data-writer': data.Writer,
          'data-actors': data.Actors,
          'data-genre': data.Genre,
          'data-title': data.Title,
          'data-year': data.Year,
          'data-plot': data.Plot,
          'data-rating': data.imdbRating,
          'data-runtime': data.Runtime,
          html: data.Title
        }).appendTo('#addedItems');

        $('<div/>', {
          'class': 'more-info icon-info button small right posRelative',
          'href': 'http://www.imdb.com/title/' + data.imdbID,
          html: ''
        }).appendTo('#list-item-' + data.imdbID);

        $('<div/>', {
          'class': 'more-info-tooltip hidden',
        }).appendTo('#list-item-' + data.imdbID + ' .more-info');

          $('<span/>', {
            'class': 'disBlock paddingBottom',
            html: $('#list-item-' + data.imdbID).attr('data-title') + ' (' + $('#list-item-' + data.imdbID).attr('data-year') + ')'
          }).appendTo('#list-item-' + data.imdbID + ' .more-info .more-info-tooltip');

          $('<span/>', {
            'class': 'disBlock paddingBottom',
            html: $('#list-item-' + data.imdbID).attr('data-plot')
          }).appendTo('#list-item-' + data.imdbID + ' .more-info .more-info-tooltip');

          $('<a/>', {
            'class': 'disBlock left',
            'href': 'http://www.imdb.com/title/' + data.imdbID,
            html: 'IMDB'
          }).appendTo('#list-item-' + data.imdbID + ' .more-info .more-info-tooltip');

        $('<img />', {
          'src': data.Poster,
          'alt': data.Title,
          'class': 'item-poster'
        }).prependTo('#list-item-' + data.imdbID);



        $('#result-list').empty();
        $('#result').addClass('hidden');

      });
  };
}

function showList(data) {

  if (!data.Response) {

    $.each(data.Search, function (index) {
      var item = $('<li/>', {
        'class': 'result-item',
        'id': data.Search[index].imdbID,
        html: data.Search[index].Title + " (" + data.Search[index].Year + ")"
      }).appendTo('#result-list');
      $('<a/>', {
        'class': 'tiny button success right addBtn',
        'href': 'javascript:;',
        html: 'Add'
      }).appendTo('#' + data.Search[index].imdbID);
    });


  } else {
    var item = $('<li/>', {
      'class': 'no-result',
      html: data.Error
    }).appendTo('#result-list');
  }
  $('#result').removeClass('hidden');
}
});